locals {
  subnet_name_tags = [ "web1", "web2", "app1", "app2", "db1", "db2" ]
  ec2_name_tags = toset(["instance1", "instance2", "instance3", "instance4"])
}