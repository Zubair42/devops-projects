provider "aws" {
    shared_config_files = ["/home/tn-103/.aws/config"]       
    shared_credentials_files = ["/home/tn-103/.aws/credentials"]
    region = var.region
  
}