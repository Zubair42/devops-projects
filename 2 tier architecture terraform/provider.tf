# --- root/DevOps/Terraform/2-tier-architecture-terraform/provider.tf
# https://gitlab.com/devops2915/terraform/2-tier-architecture-terraform.git 
# Branch name Modified

# ************************************************************
# Auther    : Zubair Khan
# Date      : May 11 2023
# Description: two-tier architecture with terraform
# - file name: provider.tf
# - Specifies provider
# ************************************************************

# PROVIDER BLOCK

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.23"
    }
  }
  required_version = ">= 1.2.0"
}

provider "aws" {
  region  = "ap-south-1"
}

