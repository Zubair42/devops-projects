# ************************************************************
# Auther    : Zubair Khan
# Date      : Jun 04 2023
# Description: Create AMI from list of axsiting ec2 images
# ************************************************************
provider "aws" {
  shared_config_files      = ["/home/tn-103/.aws/config"]
  shared_credentials_files = ["/home/tn-103/.aws/credentials"]
  region                   = "ap-south-1"
}
# local variable
locals {
  ec2_name_tags = toset(["i-0bd712f8e86ecd5c4", "i-0b3d13301733b467c", "i-0459cd1af13055eb8", "i-0293cc85c4805a539"])
#  ami_name_tags = [ "instance1", "instance2", "instance3", "instance4" ]
}
resource "aws_ami_from_instance" "ami" {
    for_each = local.ec2_name_tags
    name = "ami-backup"
    description = "ami-backup"
    source_instance_id = each.value
    tags = {
        name = each.value
    }
}
