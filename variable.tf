# aws region
variable "region" {
    type = string
    default = "ap-south-1"
  
}
# custom VPC variable
variable "vpc" {
  description = "VPC"
  type        = string
  default     = "DTS_VPC"
}
variable "dts_vpc_cidr" {
  description = "custom vpc CIDR notation"
  type        = string
  default     = "10.0.0.0/16"
}


# public subnet 1 variable
variable "DTS_subnet" {
  description = "subnet"
  type        = string
  default     = "DTS_Subnet"

}
variable "DTS_subnet_cidr" {
  description = "public subnet 1 CIDR notation"
  type        = string
  default     = "10.0.1.0/24"
}
# AZ 1
variable "az1" {
  description = "availability zone 1"
  type        = string
  default     = "ap-south-1a"
}

# ec2 instance ami for Linux
variable "ec2_instance_ami" {
  description = "ec2 instance ami id"
  type        = string
  default     = "ami-08e5424edfe926b43"
}


# ec2 instance type
variable "ec2_instance_type" {
  description = "ec2 instance type"
  type        = string
  default     = "t2.micro"
}
# ec2 name
variable "ec2_name_tags" {
  type = list(string)
  default = [ "instance1", "instance2", "instance3", "instance4" ]
  
}
###################################################################

#Name of the key pair
variable "key_pair_name" {
  type = string
  default  = "DTS-Key"
}
#Public Key to use in Key pair Generation
variable "public_key" {
  type = string
  default  = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCUQTE5VmX0OXc28DAOukBwHaj0NP85AN/uS/q1Kd8jsv78gAHuEXHxkPITlALhLZiC5UQMoqmpQI3ykfG5f6+7oLTCGS9v6ROjWs9yvNlcdTG/Pze1TXoKogqUHN3ASqBX8Gtq4wwDcB+t4niuyMIHJ0Ctt5vViFeMWrLX0lHezOKbc/kQMUd1l0WMWxamgEVHT0PTgsBAE7ErP/yMMuTG0UGICipQgvFTkZEbw/S8N1DtLMiHheUWHC+4ngAUjf713KJJ620N/mQCQx3Lm18TVCWx+Yt0HKnClEdBMObSCAVuEivFi87WNNXVmD5HDbO+XD6v5hDD95P3T/9novar"
}