# --- root/DevOps/Terraform/terraform-project-aws-001/main.tf
# https://gitlab.com/devops2915/terraform/terraform-project-aws-001.git 
# Branch name main

# ************************************************************
# Auther    : Zubair Khan
# Date      : May 25 2023
# Description: DTS architecture with terraform
# - file name: main.tf
# - 1 custom VPC
# - 1 public subnets
# - 1 EC2 t2.micro instance in each public subnet
# ************************************************************

# VPC Block
resource "aws_vpc" "vpc" {
  cidr_block = var.dts_vpc_cidr
  tags = {
    "Name" = var.vpc
  }
}

#Subnet Block
resource "aws_subnet" "subnet" {
  availability_zone = var.az1
  vpc_id = aws_vpc.vpc.id
  cidr_block = var.DTS_subnet_cidr
  tags = {
    "Name" = var.DTS_subnet
  }
  depends_on = [ aws_vpc.vpc ]
}

#Internet Gatway
resource "aws_internet_gateway" "DTS_igw" {
    tags = {
      "Name" = "DTS_igw"
    }
    depends_on = [ aws_vpc.vpc ]  
}
resource "aws_internet_gateway_attachment" "DTS_igw_attach" {
  internet_gateway_id = aws_internet_gateway.DTS_igw.id
  vpc_id              = aws_vpc.vpc.id
  depends_on = [ aws_internet_gateway.DTS_igw ]
}
# creating route table
resource "aws_route_table" "DTS_rt" {
   vpc_id = aws_vpc.vpc.id
   route {
      cidr_block = "0.0.0.0/0"
      gateway_id = aws_internet_gateway.DTS_igw.id
  }
  tags = {
      name = "DTS_rt"
  }
}
# tags are not allowed here 
# associate route table to the public subnet 1
resource "aws_route_table_association" "DTS_rt_associat" {
   subnet_id      = aws_subnet.subnet.id
   route_table_id = aws_route_table.DTS_rt.id
}

# SECURITY BLOCK

# vpc security group 
resource "aws_security_group" "DTS_sg" {
   name        = "DTS_sg"
   description = "allow inbound HTTP traffic"
   vpc_id      = aws_vpc.vpc.id

   # HTTP from vpc
   ingress {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]     
   }
   # ssh access to server from any where
   ingress {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]     
   }


  # outbound rules
  # internet access for server to anywhere
  egress {
     from_port   = 0
     to_port     = 0
     protocol    = "-1"
     cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
     name = "DTS_sg"
  }
}

# INSTANCES BLOCK - EC2 instance
# user_data = file("install_apache.sh")  
# if used with file option - get multi-line argument error 
# as echo statement is long
# 1st ec2 instance on public subnet 1
resource "aws_key_pair" "DTS-key_pair_name" {
  key_name = var.key_pair_name
  public_key = var.public_key
#   public_key = "${file("/home/tn-103/Downloads/DTS_Key.pem")}"
 
}
resource "aws_instance" "ec2" {
#  for_each = local.ec2_name_tags
   ami                     = var.ec2_instance_ami
   instance_type           = var.ec2_instance_type
   subnet_id               = aws_subnet.subnet.id
   vpc_security_group_ids  = [aws_security_group.DTS_sg.id] 
   key_name = aws_key_pair.DTS-key_pair_name.key_name
   associate_public_ip_address = "true"
   user_data = "${file("/home/tn-103/Documents/Cloud-n-devops/DevOps/Terraform/Project/terraform-project-aws-001/user_data.sh")}"
     tags = {
    name = "Ansible-node"
#      name = each.key
  }
}
