
terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.19.0"
    }
  }
}
var "bucket-name" {
    default = "s3-bucket-lifecyle-policy"
}
var "versioning" {
    default= false
}
resource "aws_s3_bucket" "s3-bucket-lifecycle" {

  bucket = var.bucket-name

  tags = {
    Enviroment : "Production"
  }
}
resource "aws_s3_bucket_lifecycle_configuration" "bucket-config" {

  bucket = aws_s3_bucket.s3-bucket-lifecycle.bucket

  rule {
    id = "archival"

    filter {
      and {
        prefix = "/"

        tags = {
          rule      = "archival"
          autoclean = "false"
        }
      }
    }

    status = "Enabled"

    transition {
      days          = 30
      storage_class = "STANDARD_IA"
    }

    transition {
      days          = 60
      storage_class = "GLACIER"
    }
  }
}
